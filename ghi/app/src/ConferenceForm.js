import React from 'react';
import { useEffect, useState} from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState("");
    const [starts, setStarts] = useState("");
    const [ends, setEnds] = useState("");
    const [description, setDescription] = useState("");
    const [presentationCount, setPresentationCount] = useState("");
    const [attendeesCount, setAttendeesCount] = useState("");
    const [location, setLocation] = useState("");

    const handleName = event => {
        setName(event.target.value);
    }
    const handleStart = event => {
        setStarts(event.target.value);
    }
    const handleEnd = event => {
        setEnds(event.target.value);
    }
    const handleDescription = event => {
        setDescription(event.target.value);
    }
    const handlePCount = event => {
        setPresentationCount(event.target.value);
    }
    const handleACount = event => {
        setAttendeesCount(event.target.value);
    }
    const handleLocation = event => {
        setLocation(event.target.value);
    }

    const handleSubmit = async event => {
        event.preventDefault();
        const postURL = 'http://localhost:8000/api/conferences/';
        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = presentationCount;
        data.max_attendees = attendeesCount;
        data.location = location;

        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type" : "application/json",
            }
        }

        const postResponse = await fetch(postURL, fetchOptions);
        if (postResponse.ok){
            console.log("Got it");
            const newPost = await postResponse.json();
            console.log(newPost);
            document.getElementById("create-conference-form").reset();
        }

    }

    const fetchLocations = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const locationResponse = await fetch(url);
        if (locationResponse.ok){
            const locationData = await locationResponse.json();
            setLocations(locationData.locations)
        } else {
            console.log(locationResponse)
        }
    }

    useEffect(() => {
        fetchLocations();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStart} placeholder="mm/dd/yy" required type="date" name="starts" id="starts" className="form-control" />
                            <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEnd} placeholder="mm/dd/yy" required type="date" name="ends" id="ends" className="form-control" />
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description">Description</label>
                            <textarea onChange={handleDescription} required name="description" id="description" rows="3" className="form-control" />
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePCount} required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                            <label htmlFor="max_presentations">Maxiumum Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleACount} required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                            <label htmlFor="max_attendees">Maxiumum attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocation} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option value={location.id} key={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ConferenceForm;
