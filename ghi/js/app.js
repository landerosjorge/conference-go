function createCard(name, description, pictureUrl, starts, ends, location) { // were returning a f`string to get more html
    const start = new Date(starts).toLocaleDateString();
    const end = new Date(ends).toLocaleDateString();
    return `
        <div class="card mb-3 shadow">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h5 class="card-title">${location}</h5>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${start} - ${end}
            </div>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.error('bad response')
        } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`; // getting detail for certain conference href = number
            const detailResponse = await fetch(detailUrl); // Getting the detail response

            if (detailResponse.ok) { // if ok
                const details = await detailResponse.json(); // set details to the json
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const dateStart = details.conference.starts;
                const dateEnds = details.conference.ends;
                const location = details.conference.location.name;
                const html = createCard(name, description, pictureUrl, dateStart, dateEnds, location); // we add them as parameters to the function so it can
                const column = document.querySelector('.row');          // create multiple cards
                column.innerHTML += html;
            }
        }

        }
    } catch (e) {
        console.error(e)
    }

});
