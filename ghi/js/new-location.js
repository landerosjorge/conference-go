window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/'; //sending a get to state API
    const response = await fetch(url); // Getting a fetch with a promise

    if (response.ok) { // if response ok
        const data = await response.json(); // the data with promise

        const selectTag = document.getElementById('state'); // selecting tag with state
        for (let state of data.states) {                    // for loop for state
            const option = document.createElement('option'); // creating value option
            option.value = state.abbreviation;               //setting the option value to state
            option.innerHTML = state.name;                   // setting the option to show name
            selectTag.appendChild(option);                   // appending it to the select tag
        }
    }
    const formTag = document.getElementById('create-location-form'); // selecting the form
    formTag.addEventListener('submit', async event => {              // adding a event listener so when someone sumbits
        event.preventDefault();                                      // prevents default since we dont want it to
        const formData = new FormData(formTag);                      // Getting the data from it and making a new one with key/value
        const json = JSON.stringify(Object.fromEntries(formData));   // Getting the object and strignify it
        const locationUrl = 'http://localhost:8000/api/locations/';  // Setting the location to API to make a post
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);     // if it goes through reset
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
        }
    })
});
